


from django.urls import path

from core import views

app_name = "core"

urlpatterns = [
    ### GRANTGOAL URLS ###
    path('list/grantgoal/', views.ListGrantGoal.as_view(), name="list_grantgoal"),
    path('detail/grantgoal/<int:pk>/', views.DetailGrantGoal.as_view(), name="detail_grantgoal"),
    path('create/grantgoal/', views.CreateGrantGoal.as_view(), name='create_grantgoal'),
]