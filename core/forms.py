

from django import forms

from .models import GrantGoal

class GrantGoalForm(forms.ModelForm):
    class Meta:
        model = GrantGoal
        fields = [
            "user",
            "grantgoal_name",
            "description",
            "days_duration",
            "state",
            "status",
            "slug"
        ]
        widgets = {
            "user": forms.Select(attrs={"class": "form-select", "type":"select"}),
            "grantgoal_name": forms.TextInput(attrs={"type": "text", "class":"form-control", "placeholder":"Escribe el nombre del grantgoal"}),
            "description": forms.Textarea(attrs={"type": "text", "class":"form-control", "placeholder":"Escribe la descripcion del grantgoal", "rows":3}),
            "days_duration": forms.NumberInput(attrs={"type": "number", "class":"form-control", "placeholder":"Escribe el numero de dias de duracion del grantgoal"}),
            "state": forms.Select(attrs={"type": "select", "class": "form-select"})
        }