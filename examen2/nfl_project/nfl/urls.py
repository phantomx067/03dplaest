from django.urls import path
from . import views

urlpatterns = [
    path('equipos/', views.lista_equipos, name='lista_equipos'),
    path('ciudades/', views.lista_ciudades, name='lista_ciudades'),
    path('', views.home, name='home'),
]
