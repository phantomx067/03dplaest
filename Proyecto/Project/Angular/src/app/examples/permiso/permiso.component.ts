import { Component, OnInit } from '@angular/core';
import { GetApiService } from '../../get-api.service';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { warn } from 'console';

export interface Items{
  idItem: number;
  nameItem: string;
}

@Component({
  selector: 'app-permiso',
  templateUrl: './permiso.component.html',
  styleUrls: ['./permiso.component.scss']
})






export class PermisoComponent implements OnInit {

  
public DatosPermiso: Array<Permiso> = [];

displayedColumns: string[] = ['idItem', 'nameItem', 'options'];
columnsToDisplay: string[] = this.displayedColumns.slice();
itemList: Items[]; 

input_id: string = "";
input_nombre: string = "";
input_c_usuario: string = "";
input_v_usuario: string = "";
input_e_usuario: string = "";
input_c_orden: string = "";
input_v_orden: string = "";
input_e_orden: string = "";
input_e_modelo: string = "";



  constructor(private api: GetApiService, private router: Router, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.mostrarPermisos();
  }
  mostrarPermisos() {
    this.api.apiConsultPermiso().subscribe(data => {
      this.DatosPermiso = [];
      for (let index = 0; index < data[0].datos.length; index++) {
        this.DatosPermiso.push({
          id: data[0].datos[index].id,
          nombre: data[0].datos[index].nombre,
          c_usuario: data[0].datos[index].c_usuario,
          v_usuario: data[0].datos[index].v_usuario,
          e_usuario: data[0].datos[index].e_usuario,
          c_orden: data[0].datos[index].c_orden,
          v_orden: data[0].datos[index].v_orden,
          e_orden: data[0].datos[index].e_orden,
          e_modelo: data[0].datos[index].e_modelo,
        });
        console.warn(this.DatosPermiso);
      }
     
      
      
    });
  }
  
  
}

export interface Permiso {
  id: string,
  nombre: string,
  c_usuario: boolean,
  v_usuario: boolean,
  e_usuario: boolean,
  c_orden: boolean,
  v_orden: boolean,
  e_orden: boolean,
  e_modelo: boolean,
}