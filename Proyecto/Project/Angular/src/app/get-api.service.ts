import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { log } from 'console';

@Injectable({
  providedIn: 'root'
})
export class GetApiService {

  constructor(private http:HttpClient) { }

// -------------------CONSULT EMPLEADOS--------------------------

// apiCall(){
//   console.log("Antes de return ------------------------------------------------------------------------------------------")
//   return this.http.get("api/consultEmpleados")
//   console.log("Despues de return ------------------------------------------------------------------------------------------")

// }  
// -------------------LOGIN EMPLEADOS--------------------------

apiLoginEmp(user:string,pass:string){
        console.log("Entro al metodo");
        
        var datos = {
          "user": user,
          "password": pass
        }
  return this.http.post("api/login",datos)
}  
// -------------------CONSULT EMPLEADOS--------------------------

apiConsultEmpleado(){
  return this.http.get("api/consultEmpleado")
  }
// -------------------CONSULT EMPLEADOS-DEPARTAMENTOS--------------------------

apiConsultEmpleadoJoin() {
  return this.http.get("api/consultEmpleadoJoin");
}

// -------------------INSERT EMPLEADOS--------------------------

apiCrearEmpleado(nombre:string, apellidop:string, apellidom:string, password:string, permiso:number, puesto:string, departamento:number ){
  var datos = {
    "nombre": nombre,
    "apellidop": apellidop,
    'apellidom': apellidom,
    'password': password,
    'permiso': permiso,
    'puesto': puesto,
    'departamento': departamento
  }
return this.http.post("api/crearEmpleado",datos)
}
// -------------------UPDATE EMPLEADOS--------------------------

apiActualizarEmpleado(id:number, nombre:string, apellidop:string, apellidom:string, password:string, permiso:number, puesto:string, departamento:number ){
  var datos = {
    "id": id,
    "nombre": nombre,
    "apellidop": apellidop,
    'apellidom': apellidom,
    'password': password,
    'permiso': permiso,
    'puesto': puesto,
    'departamento': departamento
  }
return this.http.put("api/actualizarDepartamento",datos)
}

// -------------------DESHABILITAR EMPLEADOS--------------------------

apiDeshabilitarEmpleado(id:number){
  var datos = {
    "id": id
  }
  console.log("id api: "+id);
  
return this.http.put("api/deshabilitarEmpleado",datos)
}

// -------------------CONSULT DEPARTAMENTOS--------------------------

apiConsultDepartamento(id:number){
  var id_:number = 0
  if(id != 0)
    id_=id;
return this.http.get("api/consultDepartamento?id="+id_)
}  

// -------------------INSERT DEPARTAMENTOS--------------------------


apiCrearDepartamento(nombre:string,descripcion:string){
  var datos = {
    "nombre": nombre,
    "descripcion": descripcion
  }
return this.http.post("api/crearDepartamento",datos)
}  

// -------------------ACTUALIZAR DEPARTAMENTOS--------------------------

apiActualizarDepartamento(id:string,nombre:string,descripcion:string){
  var datos = {
    "id": id,
    "nombre": nombre,
    "descripcion": descripcion
  }
return this.http.put("api/actualizarDepartamento",datos)
}

// -------------------DESHABILITAR DEPARTAMENTOS--------------------------

apiDeshabilitarDepartamento(id:string){
  var datos = {
    "id": id
  }
  console.log("id api: "+id);
  
return this.http.put("api/deshabilitarDepartamento",datos)
}

// -------------------CONSULT PUESTOS--------------------------

apiConsultPuesto(){
  return this.http.get("api/consultPuesto")
  }

// -------------------INSERT PUESTOS--------------------------


  apiCrearPuesto(id:string,nombre:string,descripcion:string){
    var datos = {
      "id": id,
      "nombre": nombre,
      "descripcion": descripcion
    }
  return this.http.post("api/crearPuesto",datos)
  } 

// -------------------ACTUALIZAR PUESTOS--------------------------


  apiActualizarPuesto(id:string, nombre:string,descripcion:string){
    var datos = {
      "id": id,
      "nombre": nombre,
      "descripcion": descripcion
    }
  return this.http.put("api/actualizarPuesto",datos)
  }

// -------------------DESHABILITAR PUESTOS--------------------------

apiDeshabilitarPuesto(id:string){
  var datos = {
    "id": id
  }
  console.log("id api: "+id);
return this.http.put("api/deshabilitarPuesto",datos)
}


// -------------------CONSULT PERMISOS--------------------------


apiConsultPermiso(){
  console.log("Entro al metdodo");
  
  return this.http.get("api/consultPermiso")
  }

}
